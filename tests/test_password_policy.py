from password_policy.calculator import get_human_strength, get_machine_strength
from password_policy.data import PCP
from password_policy import __version__

def test_version():
  assert __version__ == '0.2.0'
  
  
def test_loads_dumps():
  tests = [
    '{"min_length": 6}',
    '{"min_length": 8, "max_length": 20, "require_subset": {"count": 2}}',
    '{"min_length": 8, "max_consecutive": 7}'
  ]
  
  for test in tests:
    pcp = PCP.loads(test)
    pcp.validate()
    assert pcp.dumps() == test


def test_machine_strength():
  tests = [
    ('{"min_length": 6}', 367545945312),
    ('{"min_length": 6, "require": ["alphabet", "digits", "symbols"]}', 157941927000),
    ('{"min_length": 8, "max_length": 20, "require_subset": { "count": 2 }}', 3316190075071616),
    ('{"min_length": 8, "max_consecutive": 7}', 3317102156445265)
  ]
  
  for test in tests:
    pcp = PCP.loads(test[0])
    strength = get_machine_strength(pcp)
    assert strength == test[1]


def test_human_strength():
  tests = [
    ('{"min_length": 6}', 154457888),
    ('{"min_length": 6, "require": ["alphabet", "digits", "symbols"]}', 36192499200),
    ('{"min_length": 8, "max_length": 20, "require_subset": { "count": 2 }}', 14741460830720),
    ('{"min_length": 8, "max_consecutive": 7}', 104413532275)
  ]
  
  for test in tests:
    pcp = PCP.loads(test[0])
    strength = get_human_strength(pcp)
    assert strength == test[1]

