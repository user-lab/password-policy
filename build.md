# Build system
This project is built using `poetry`. To install dependencies, use `poetry install`. To update dependencies, use `poetry update`. To run tests, use `poetry run pytest`.

This library is designed to work with python 3.6.1 and above. Internally, we use pyenv to get the correct version of python.
